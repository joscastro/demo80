package com.nuevatel.deafult;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeafultApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeafultApplication.class, args);
	}

}
