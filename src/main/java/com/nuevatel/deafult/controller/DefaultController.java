package com.nuevatel.deafult.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/entityDeafault")
public class DefaultController {
    
    @GetMapping
    public String getDefault(){
        return "Hola a todos";
    }
}
